<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Department;
use App\Models\Equip;
use Illuminate\Http\Request;

class EquipController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:Listar Equipos')->only('index');
        $this->middleware('can:Crear Equipos')->only('create', 'store');
        $this->middleware('can:Editar Equipos')->only('edit', 'update');
        $this->middleware('can:Borrar Equipos')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::all();
        $equips = Equip::all();

        return view('admin.equips.index', compact('equips', 'brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::orderBy('name')->pluck('name', 'id');
        $departments = Department::orderBy('name')->pluck('name', 'id');

        return view('admin.equips.create', compact('brands', 'departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'serie' => 'required'
        ]);

        Equip::create([
            'serie' => $request->serie,
            'mac' => $request->mac,
            'ip' => $request->ip,
            'brand_id' => $request->brand_id,
            'department_id' => $request->department_id
        ]);

        return redirect()->route('admin.equips.index')->with('info', 'Equipo creado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Equip $equip)
    {
        return view('admin.equips.show', compact('equip'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Equip $equip)
    {
        $brands = Brand::orderBy('name')->pluck('name', 'id');
        $departments = Department::orderBy('name')->pluck('name', 'id');

        return view('admin.equips.edit', compact('equip', 'brands', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Equip $equip)
    {
        $request->validate([
            'serie' => 'required'
        ]);

        $equip->update([
            'serie' => $request->serie,
            'mac' => $request->mac,
            'brand_id' => $request->brand_id,
            'department_id' => $request->department_id
        ]);

        return redirect()->route('admin.equips.index')->with('info', 'Equipo actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Equip $equip)
    {
        $equip->delete();
        return redirect()->route('admin.equips.index')->with('info', 'Equipo eliminado');
    }
}
