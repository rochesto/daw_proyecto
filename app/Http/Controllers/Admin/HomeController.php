<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Ticket;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $tickets_abiertos = Ticket::where('state_id', '!=', '4')->get()->count();
        $tickets_terminados = Ticket::where('state_id', '4')->get()->count();
        $ticket_ultimo = Ticket::latest()->first();

        $datos = [
            'tickets' => [
                'abiertos' => $tickets_abiertos,
                'terminados' => $tickets_terminados,
                'ultimo' => $ticket_ultimo
            ],

        ];


        return view('admin.index', compact('datos'));
    }


}
