<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Comment;
use App\Models\Equip;
use App\Models\State;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{

    public function __construct()
    {
        $this->middleware('can:Listar Tickets')->only('index');
        $this->middleware('can:Crear Tickets')->only('create', 'store');
        $this->middleware('can:Editar Tickets')->only('edit', 'update');
        $this->middleware('can:Borrar Tickets')->only('destroy');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tickets.index', [
            'tickets' => Ticket::where('department_id', Auth::user()->department_id)->where('deleted', false)->orderBy('created_at', 'desc')->paginate(7),
            'states' => State::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $equips = Equip::orderBy('serie')->pluck('serie', 'id');
        $brands = Brand::all();

        return view('tickets.new', compact('equips', 'brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'subject' => 'required',
            'body' => 'required',
            'equip_id' => 'required'
        ]);

        $ticket = Ticket::create([
            'subject' => $request->subject,
            'body' => $request->body,
            'equip_id' => $request->equip_id,
            'department_id' => Auth::user()->department_id,
            'state_id' => State::where('name', 'nuevo')->first()->id,

        ]);

        return redirect()->route('ticket.show', $ticket)
            ->with('success', 'Product created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = Ticket::where('id', $id)->first();
        $states = State::all();
        $comments = Comment::where('ticket_id', $ticket->id)->orderBy('Created_at')->get();

        return view('tickets.show', compact('ticket', 'comments', 'states'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ticket = Ticket::where('id', $id)->first();

        $ticket->update([
            'state_id' => $request->state_id
        ]);
        return redirect()->route('ticket.show', $ticket)->with('info', 'Ticket terminado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        $ticket->update([
            'state_id' => 4,
            'deleted' => true
        ]);

        return redirect()->route('ticket.index')->with('info', 'ELiminado con exito');
    }
}
