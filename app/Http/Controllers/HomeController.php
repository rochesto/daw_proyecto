<?php

namespace App\Http\Controllers;

use App\Models\Notice;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __invoke()
    {
        $notices = Notice::latest()->take(5)->where('publish', true)->get();

        return view('welcome', compact('notices'));
    }
}
