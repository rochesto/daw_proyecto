<?php

namespace App\Http\Livewire;

use App\Models\Notice;
use Livewire\Component;

class CardNotices extends Component
{
    public $notice;


    public function mount(Notice $notice){
        $this->notice = $notice;
    }

    public function render()
    {
        return view('livewire.card-notices');
    }
}
