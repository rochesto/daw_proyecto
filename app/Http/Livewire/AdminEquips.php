<?php

namespace App\Http\Livewire;

use App\Models\Equip;
use Livewire\Component;

use Livewire\WithPagination;

class AdminEquips extends Component
{
    use WithPagination;

    protected $paginationTheme = "bootstrap";

    public $search;

    public function render()
    {
        $equips = Equip::where('mac', 'LIKE', '%' . $this->search . '%')
            ->orWhere('serie', 'LIKE', '%' . $this->search . '%')
            ->orWhere('ip', 'LIKE', '%' . $this->search . '%')
            ->paginate(8);

        return view('livewire.admin-equips', compact('equips'));
    }

    // Para que al buscar con el buscador, busque en cualquier pagina necesitamos resetear page
    public function limpiarPaginate(){
        $this->reset('page');
    }
}
