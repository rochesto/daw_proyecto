<?php

namespace App\Http\Livewire;

use App\Models\State;
use App\Models\Ticket;
use Livewire\Component;

class CardTicket extends Component

{

    public $ticket;
    public $states;

    public function mount(Ticket $ticket){
        $this->ticket = $ticket;
        $this->states = State::orderBy('name')->pluck('name', 'id');
    }

    public function render()
    {
        return view('livewire.card-ticket');
    }
}
