<?php

namespace App\Http\Livewire;

use App\Models\State;
use App\Models\Ticket;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

use Livewire\WithPagination;


class AdminTicket extends Component
{

    use WithPagination;

    protected $paginationTheme = "bootstrap";

    public $search;

    public function render()
    {

        $tickets = Ticket::where('subject', 'LIKE', '%' . $this->search . '%')
        ->orderBy('updated_at', 'desc')->paginate(8);
        $states = State::all();

        return view('livewire.admin-ticket', compact('tickets', 'states'));
    }

    // Para que al buscar con el buscador, busque en cualquier pagina necesitamos resetear page
    public function limpiarPaginate(){
        $this->reset('page');
    }
}
