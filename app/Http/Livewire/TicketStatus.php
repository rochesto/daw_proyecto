<?php

namespace App\Http\Livewire;

use Livewire\Component;

class TicketStatus extends Component
{
    public $comments;
    public $ticket;

    public function render()
    {
        return view('livewire.ticket-status');
    }

    public function mount($comments, $ticket){
        $this->comments = $comments;
        $this->ticket = $ticket;
    }
}
