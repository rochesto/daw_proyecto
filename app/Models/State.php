<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    // Relacion 1 a muchos inversa
    public function ticket(){
        return $this->hasMany('App\Models\Ticket');
    }

}
