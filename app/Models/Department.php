<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    // Relacion 1 a muchos inversa
    public function user(){
        return $this->hasMany('App\Models\User');
    }

    public function ticket()
    {
        return $this->hasMany('App\Models\Ticket');
    }

    public function equip()
    {
        return $this->hasMany('App\Models\Equip');
    }
}
