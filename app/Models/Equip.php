<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Equip extends Model
{
    use HasFactory;

    protected $guarded = ['id'];


    // Relacion 1 a muchos
    public function brand()
    {
        return $this->belongsTo('App\Models\Brand');
    }

    public function department()
    {
        return $this->belongsTo('App\Models\Department');
    }

    // Relacion 1 a muchos inversa
    public function ticket()
    {
        return $this->hasMany('App\Models\Ticket');
    }

}
