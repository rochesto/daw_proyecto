<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    // Relacion 1 a muchos
    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    public function department()
    {
        return $this->belongsTo('App\Models\Department');
    }

    public function equip()
    {
        return $this->belongsTo('App\Models\Equip');
    }


    // Relacion 1 a muchos inversa
    public function comment(){
        return $this->belongsToMany('App\Models\Comment');
    }
}
