<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\NoticeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', HomeController::class)->name('home');

Route::resource('ticket', TicketController::class)->middleware(['auth:sanctum', 'verified'])->names('ticket');
Route::resource('comment', CommentController::class)->middleware(['auth:sanctum', 'verified'])->names('comments');
Route::resource('notice', NoticeController::class)->middleware(['auth:sanctum', 'verified'])->names('notices');



Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
