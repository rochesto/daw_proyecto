<?php

use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\DepartmentController;
use App\Http\Controllers\Admin\EquipController;
use App\Http\Controllers\Admin\TicketController;



use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->middleware('can:Ver Panel')->name('index');

// Route::resource('users', UserController::class)->only(['index', 'edit', 'update'])->names('users');

Route::resource('tickets', TicketController::class)->names('tickets');

Route::resource('users', UserController::class)->names('users');

Route::resource('roles', RoleController::class)->names('roles');

Route::resource('departments', DepartmentController::class)->names('departments');

Route::resource('equips', EquipController::class)->names('equips');

