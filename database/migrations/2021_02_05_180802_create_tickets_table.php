<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string('subject');
            $table->text('body');
            $table->unsignedBigInteger('equip_id');
            $table->unsignedBigInteger('department_id')->nullable();
            $table->unsignedBigInteger('state_id')->nullable();
            $table->string('notes')->nullable();
            $table->boolean('deleted')->default(false);
            $table->timestamps();


        });

        Schema::table('tickets', function (Blueprint $table){
            $table->foreign('equip_id')->references('id')->on('equips')->onDelete('cascade');
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('set null');
            $table->foreign('state_id')->references('id')->on('states')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
