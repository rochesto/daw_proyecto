<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;



class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Administrador',
            'email' => 'admin@ticket.tic',
            'password' => bcrypt('12345678'),
        ])->assignRole('Administrador');

        User::create([
            'name' => 'tecnico',
            'email' => 'tecnico@ticket.tic',
            'password' => bcrypt('12345678'),
        ])->assignRole('Tecnico');

        User::create([
            'name' => 'redactor',
            'email' => 'redactor@ticket.tic',
            'password' => bcrypt('12345678'),
        ])->assignRole('Redactor');

        User::create([
            'name' => 'girona',
            'email' => 'girona@ticket.tic',
            'password' => bcrypt('12345678'),
            'department_id' => '1'
        ])->assignRole('Admin Unidad');

        User::create([
            'name' => 'barcelona',
            'email' => 'barcelona@ticket.tic',
            'password' => bcrypt('12345678'),
            'department_id' => '2'
        ])->assignRole('Admin Unidad');

        User::create([
            'name' => 'lerida',
            'email' => 'lerida@ticket.tic',
            'password' => bcrypt('12345678'),
            'department_id' => '3'
        ])->assignRole('Admin Unidad');

        User::create([
            'name' => 'tarragona',
            'email' => 'tarragona@ticket.tic',
            'password' => bcrypt('12345678'),
            'department_id' => '4'
        ])->assignRole('Admin Unidad');

        User::factory(10)->create();
    }
}
