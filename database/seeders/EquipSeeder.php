<?php

namespace Database\Seeders;

use App\Models\Equip;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Factories\Factory;

class EquipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Equip::create([
            'serie' => '4CE0460D0G',
            'mac' => '0C:4E:94:23:54:FF',
            'ip' => '10.10.0.2',
            'brand_id' => '1',
            'department_id' => '2'
        ]);

        Equip::create([
            'serie' => '4CE0460D12',
            'mac' => '0C:4E:94:23:54:FE',
            'ip' => '10.10.0.3',
            'brand_id' => '3',
            'department_id' => '2'
        ]);

        Equip::create([
            'serie' => '4CE0460D1234',
            'mac' => '0C:4E:94:23:54:FC',
            'ip' => '10.10.0.3',
            'brand_id' => '2',
            'department_id' => '1'
        ]);

        Equip::create([
            'serie' => '4CE0460D23',
            'mac' => '0C:4E:94:23:54:FA',
            'ip' => '10.10.0.2',
            'brand_id' => '3',
            'department_id' => '1'
        ]);

        Equip::create([
            'serie' => '4CE0460Dd32',
            'mac' => '0C:4E:94:23:54:F9',
            'ip' => '10.10.0.5',
            'brand_id' => '3',
            'department_id' => '3'
        ]);

        Equip::create([
            'serie' => '1234CE0460D',
            'mac' => '0C:4E:94:23:54:F8',
            'ip' => '10.10.0.23',
            'brand_id' => '1',
            'department_id' => '3'
        ]);

        Equip::create([
            'serie' => 'fde234CE0460D',
            'mac' => '0C:4E:94:23:54:F7',
            'ip' => '10.10.0.25',
            'brand_id' => '1',
            'department_id' => '4'
        ]);

        Equip::factory(50)->create();
    }
}
