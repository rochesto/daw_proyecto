<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::create([
            'name' => 'rec. humanos',
            'email' => 'rhumanos-girona@tic.tic',
            'phone' => '972001122'
        ]);

        Department::create([
            'name' => 'secretaria girona',
            'email' => 'secretaria-girona@tic.tic',
            'phone' => '972001123'
        ]);

        Department::create([
            'name' => 'administracion girona',
            'email' => 'administracion-girona@tic.tic',
            'phone' => '972001124'
        ]);


        Department::create([
            'name' => 'logistica girona',
            'email' => 'logistica-girona@tic.tic',
            'phone' => '972001125'
        ]);

        Department::factory(10)->create();
    }

}
