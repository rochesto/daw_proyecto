<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Role::create([
            'name' => 'Administrador'
        ])->permissions()->attach([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26]);

        Role::create([
            'name' => 'Admin Unidad'
        ])->syncPermissions(['Ver Panel', 'Crear Tickets', 'Listar Tickets', 'Editar Tickets', 'Borrar Tickets']);

        Role::create([
            'name' => 'Tecnico'
        ])->syncPermissions(['Ver Panel', 'Crear Equipos', 'Listar Equipos', 'Editar Equipos', 'Borrar Equipos', 'Crear Tickets', 'Listar Tickets', 'Editar Tickets', 'Borrar Tickets']);

        Role::create([
            'name' => 'Redactor'
        ])->syncPermissions(['Ver Panel', 'Crear Novedades', 'Listar Novedades', 'Editar Novedades', 'Borrar Novedades']);

    }
}
