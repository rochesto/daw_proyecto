<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Administrador. Si el usuario tiene este rol, podra modificar sus recursos y los de otras dependencias
        Permission::create([
            'name' => 'Administrador'
        ]);

        // Panel de administracion
        Permission::create([
            'name' => 'Ver Panel'
        ]);

        // Usuarios
        Permission::create([
            'name' => 'Crear Usuarios'
        ]);
        Permission::create([
            'name' => 'Listar Usuarios'
        ]);
        Permission::create([
            'name' => 'Editar Usuarios'
        ]);
        Permission::create([
            'name' => 'Borrar Usuarios'
        ]);

        // Roles
        Permission::create([
            'name' => 'Crear Roles'
        ]);
        Permission::create([
            'name' => 'Listar Roles'
        ]);
        Permission::create([
            'name' => 'Editar Roles'
        ]);
        Permission::create([
            'name' => 'Borrar Roles'
        ]);

        // Novedades
        Permission::create([
            'name' => 'Crear Novedades'
        ]);
        Permission::create([
            'name' => 'Listar Novedades'
        ]);
        Permission::create([
            'name' => 'Editar Novedades'
        ]);
        Permission::create([
            'name' => 'Borrar Novedades'
        ]);

        // Tickets. Crea y edita tickets propios
        Permission::create([
            'name' => 'Crear Tickets'
        ]);
        Permission::create([
            'name' => 'Listar Tickets'
        ]);
        Permission::create([
            'name' => 'Editar Tickets'
        ]);
        Permission::create([
            'name' => 'Borrar Tickets'
        ]);

        // Equipos
        Permission::create([
            'name' => 'Crear Equipos'
        ]);
        Permission::create([
            'name' => 'Listar Equipos'
        ]);
        Permission::create([
            'name' => 'Editar Equipos'
        ]);
        Permission::create([
            'name' => 'Borrar Equipos'
        ]);

        // Unidades
        Permission::create([
            'name' => 'Crear Unidades'
        ]);
        Permission::create([
            'name' => 'Listar Unidades'
        ]);
        Permission::create([
            'name' => 'Editar Unidades'
        ]);
        Permission::create([
            'name' => 'Borrar Unidades'
        ]);

    }
}
