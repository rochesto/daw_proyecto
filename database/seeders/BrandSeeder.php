<?php

namespace Database\Seeders;

use App\Models\Brand;
use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Brand::create([
            'name' => 'HP',
            'model' => '8200'
        ]);

        Brand::create([
            'name' => 'acer',
            'model' => '8300'
        ]);

        Brand::create([
            'name' => 'broder',
            'model' => 'elitedesk'
        ]);

        Brand::create([
            'name' => 'fujitsu',
            'model' => '1100'
        ]);

        Brand::create([
            'name' => 'Cannon',
            'model' => '3212'
        ]);

        Brand::create([
            'name' => 'msi',
            'model' => '8200'
        ]);

        Brand::create([
            'name' => 'SONY',
            'model' => '8200'
        ]);


        Brand::factory(20)->create();
    }
}
