<?php

namespace Database\Factories;

use App\Models\Equip;
use App\Models\State;
use App\Models\Ticket;
use App\Models\Department;
use Illuminate\Database\Eloquent\Factories\Factory;

class TicketFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ticket::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'subject' => $this->faker->sentence(10),
            'body' => $this->faker->paragraph(4),
            'equip_id' => Equip::all()->random()->id,
            'department_id' => Department::all()->random()->id,
            'state_id' => State::all()->random()->id,
            'notes' => $this->faker->text()
        ];
    }
}
