<?php

namespace Database\Factories;

use App\Models\Equip;
use App\Models\Brand;
use App\Models\Department;
use Illuminate\Database\Eloquent\Factories\Factory;

class EquipFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Equip::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'serie' => $this->faker->uuid,
            'brand_id' => Brand::all()->random()->id,
            'department_id' => Department::all()->random()->id,
            'mac' => $this->faker->uuid,
            'ip' => $this->faker->uuid
        ];
    }
}
