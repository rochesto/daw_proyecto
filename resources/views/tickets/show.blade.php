<x-app-layout>

    @include('tickets.navigation-menu')

    <section class="mt-10 max-w-7xl mx-auto px-4 sm:px-6 ">
        <h1 class="text-gray-700 text-center text-4xl font-bold mb-6">Ticket</h1>

        @livewire('card-ticket', ['ticket' => $ticket], key($ticket->id))
        {{-- Comentarios --}}
        @livewire('ticket-status', ['comments' => $comments, 'ticket' => $ticket], key($comment->id))

    </section>

</x-app-layout>
