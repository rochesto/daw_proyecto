<x-app-layout>

    @include('tickets.navigation-menu')

    <!-- component -->
<!-- component -->
<div class="mt-10 max-w-7xl mx-auto px-4 sm:px-6 ">
    <div class="bg-white bg-gradient-to-b from-gray-200 rounded-lg shadow-md mb-5">
        <div class="container mx-auto">
            <div class="w-full max-w-2xl p-3 mx-auto text-gray-800 font-bold">
                <h2 class="text-center font-bold mb-1 text-2xl">Nueva incidencia</h2>
                {!! Form::open(['route' => 'ticket.store']) !!}
                <div class="my-2">
                    {!! Form::label('subject', 'Asunto') !!}
                    {!! Form::text('subject', null, ['class' => 'rounded-lg' . ($errors->has('subject') ? 'is-invalid' : ''), 'size' => '50', 'placeholder' => 'Averia...']) !!}
                </div>
                <div>
                    {!! Form::label('department', 'Departamento:' ) !!}
                    {{  Auth::user()->department_id ? Auth::user()->department->name : 'Sin departamento asignado' }}
                </div>
                <div class="my-2">
                    {!! Form::label('equip_id', 'Equipo') !!}
                    {!! Form::select('equip_id', $equips, ['class' => 'rounded-lg']) !!}
                </div class="my-2">
                    {!! Form::textarea('body', '', ['placeholder' => 'Descripción ...']) !!}
                    <script> CKEDITOR.replace( 'body' ); </script>
                </div>
                <div class="text-center">
                {!! Form::submit('Crear', ['class' => 'bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

</x-app-layout>
