
{{-- Creamos array con los diferentes links para poder usarlo en los diferentes menus responsive --}}
@php
    $nav_links = [
        [
            'name' => 'Todos',
            'route' => route('ticket.index'),
            'active' => request()->routeIs('ticket.index')
        ],
        [
            'name' => 'Nuevo',
            'route' => route('ticket.create'),
            'active' => request()->routeIs('ticket.index')
        ],

    ]
@endphp
<nav x-data="{ open: false }" class="bg-white bg-gradient-to-b from-gray-100 border-b border-gray-100 shadow mt-3 flex justify-center items-baseline flex-wrap">
    <!-- Primary Navigation Menu -->
    <div class="max-w-7xl px-4 sm:px-6 lg:px-8">
        <div class="flex h-16">
            <div class="flex">

                <!-- Navigation Links -->
                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">


                    @foreach ($nav_links as $link)
                        <x-jet-nav-link href="{{ $link['route'] }}" :active="$link['active']">
                            {{ __($link['name']) }}
                        </x-jet-nav-link>
                    @endforeach
                </div>
            </div>

            <!-- Hamburger -->
            <div class="-mr-2 flex items-center sm:hidden">
                <button @click="open = ! open" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                    <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                        <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>
        </div>
    </div>

    <!-- Responsive Navigation Menu -->
    <div :class="{'block': open, 'hidden': ! open}" class="hidden sm:hidden">
        <div class="pt-2 pb-3 space-y-1">
            @foreach ($nav_links as $nav_link)
                <x-jet-responsive-nav-link href="{{ $nav_link['route'] }}" :active="$nav_link['active']">
                    {{ __($nav_link['name']) }}
                </x-jet-responsive-nav-link>
            @endforeach

        </div>
    </div>
</nav>
