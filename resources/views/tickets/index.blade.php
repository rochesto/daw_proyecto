<x-app-layout>

    @include('tickets.navigation-menu')

                    {{-- seccion principal --}}
    <section class="mt-10 max-w-7xl mx-auto px-4 sm:px-6 ">
        <h1 class="text-gray-700 text-center text-4xl font-bold mb-6">Tickets</h1>

        <div>
            @foreach ($tickets as $ticket)

                @livewire('card-ticket', ['ticket' => $ticket], key($ticket->id))

            @endforeach
        </div>
        <div class="mb-2">
            {{ $tickets->links() }}
        </div>
    </section>


</x-app-layout>
