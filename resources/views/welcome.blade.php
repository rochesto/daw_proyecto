<x-app-layout>
    <section class="bg-cover" style="background-image: url({{ asset('img/keyboard.jpg') }})">
        <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 py-24">
            <h1 class="text-black font-bold text-4xl mb-4">Gestor de tickets</h1>
            <p class="text-black font-bold  text-lg m-1 ">Gestiona todas tus averías de forma facil y sencilla</p>
        </div>
    </section>

    <section class="mt-10 max-w-7xl mx-auto px-4 sm:px-6 ">
        <h1 class="text-gray-700 text-center text-4xl font-bold mb-6">Novedades</h1>

        <div class="">
            @foreach ($notices as $notice)
            <x-post>
                <x-slot name="title">
                    {{ $notice->title }}
                </x-slot>
                <x-slot name="autor">
                    {{ $notice->user->name }}
                </x-slot>
                <x-slot name="date">
                    {{ $notice->updated_at->format('d M yy') }}
                </x-slot>
                <x-slot name="icon">
                    {{ asset('img/icons/man_smoke.png') }}
                </x-slot>
                {!! $notice->body !!}
            </x-post>
            @endforeach

        </div>

    </section>

</x-app-layout>
