<div>
    <div class="bg-gray-100 rounded-lg shadow-md mb-5 grid grid-cols-3 gap-4">

        <div class="mt-2 px-3 col-span-2">
            <p class="text-2xl text-gray-800 font-extrabold" >{{$notice->title}}</p>
        </div>
        
        @can('Editar Novedades')
            <div class="text-right mr-3">
                <a href="{{ route('notices.edit', $notice) }}"><button class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded">
                    Editar
                </button></a>
            </div>
        @endcan


        <div class="col-span-3">
            <div class="p-3 mx-3 rounded-md bg-gray-200 font-bold grid grid-cols-2">
                <div>
                    <p>Creado: <span class="font-light">{{ $notice->created_at }}</span></p>
                </div>
                <div>
                    <p> Autor: <span class="font-light">{{ $notice->user_id  ? $notice->user->name : ''  }}</span></p>
                </div>
            </div>
        </div>

        <div class="col-span-2 px-3 mb-5">
            <p class="mt-2 text-gray-800 font-semibold">{!! $notice->body !!}</p>
        </div>
        <div class="text-right  mr-3">
            @can('Borrar Novedades')
                <form action="{{ route('notices.destroy', $notice) }}" method="POST">
                    @csrf
                    @method('delete')
                    <button class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded" type="submit" onclick="return confirm('Estas seguro?')">Eliminar</button>
                </form>
            @endcan

        </div>

    </div>
</div>

