<div>
    <a href="{{ route('admin.equips.create') }}"><button class="btn btn-outline-dark mb-3">Nuevo Equipo</button></a>

    <div class="card">
        <input wire:keydown="limpiarPaginate" wire:model="search" class="form-control" type="text" placeholder="Buscar" >
    </div>
    @if (session('info'))
        <div class="alert alert-primary" role="alert">
            <strong>Éxito!</strong> {{ session('info') }}
        </div>
    @endif
        <table class="table table-striped">
            <thead>
                <tr class="bg-primary">
                    <th scope="col">Serie</th>
                    <th scope="col">MAC</th>
                    <th scope="col">IP</th>
                    <th scope="col">Marca</th>
                    <th scope="col">Departamento</th>
                    <th scope="col">Editar</th>
                    <th scope="col">Eliminar</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($equips as $equip)
                    <tr>
                        <th scope="row">{{ $equip->serie }}</td>
                        <td  >{{ $equip->mac }}</td>
                        <td  >{{ $equip->ip }}</td>
                        <td  >{{ $equip->brand->name }}</td>
                        <td  >{{ $equip->department->name }}</td>
                        <td  >
                            <a href="{{ route('admin.equips.edit', $equip) }}"><button class="btn btn-info">
                                <i class="fas fa-edit"></i>
                            </button></a>
                        </td>
                        <td  >
                            <form action="{{ route('admin.equips.destroy', $equip) }}" method="POST">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger" type="submit" onclick="return confirm('Estas seguro?')"><i class="fas fa-trash-alt "></i></button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>No hay ningún equipo.</td>
                    </tr>
                @endforelse

            </tbody>
        </table>
        <div class="card-footer">
            {{ $equips->links() }}
        </div>

</div>
