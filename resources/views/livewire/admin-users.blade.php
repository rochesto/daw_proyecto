<div>
    <a  href="{{ route('admin.users.create') }}"><button class="btn btn-outline-dark mb-3">Nuevo Usuario</button></a>

    <div class="card">
        <input wire:keydown="limpiarPaginate" wire:model="search" class="form-control" type="text" placeholder="Buscar" >
    </div>
    @if (session('info'))
        <div class="alert alert-primary" role="alert">
            <strong>Éxito!</strong> {{ session('info') }}
        </div>
    @endif
    <div class="card">
        @if ($users->count())
            <table class="table table-striped">
            <thead>
                <tr class="bg-primary">
                    <th  scope="col">nombre</th>
                    <th  scope="col">email</th>
                    <th  scope="col">unidad</th>
                    <th  scope="col">Editar</th>
                    <th  scope="col">Eliminar</th>
                    <th  scope="col">Roles</th>

                </tr>
            </thead>
            <tbody>
                @forelse ($users as $user)
                    <tr class="">
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ isset($user->department) ? $user->department->name : '' }}</td>
                        <td>
                            <a href="{{ route('admin.users.edit', $user) }}"><button class="btn btn-info">
                                <i class="fas fa-edit"></i>
                            </button></a>
                        </td>
                        <td>
                            <form action="{{ route('admin.users.destroy', $user) }}" method="POST">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger" type="submit" onclick="return confirm('Está seguro?')"><i class="fas fa-trash-alt "></i></button>
                            </form>
                        </td>
                        <td>
                            @forelse ($user->roles as $rol)
                                - {{ $rol->name }}
                            @empty
                                Sin rol asignado
                            @endforelse
                        </td>
                    </tr>
                @empty
                        <tr>
                            <td>No hay ningún usuario.</td>
                        </tr>
                    @endforelse

                </tbody>
            </table>
        </div>
        <div class="card-footer">
            {{ $users->links() }}
        </div>
        @else
        <div class="card-body">
            <strong>No hay registros</strong>
        </div>
        @endif
    </div>
</div>
