<div>
    <a  href="{{ route('ticket.create') }}"><button class="btn btn-outline-dark mb-3">Nuevo Ticket</button></a>

    <div class="card">
        <input wire:keydown="limpiarPaginate" wire:model="search" class="form-control" type="text" placeholder="Buscar" >
    </div>
    @if (session('info'))
        <div class="alert alert-primary" role="alert">
            <strong>Éxito!</strong> {{ session('info') }}
        </div>
    @endif
    <div class="card">
        @if ($tickets->count())

        <table class="table table-striped">
        <thead>
            <tr class="bg-primary">
                <th  scope="col">Asunto</th>
                <th  scope="col">Equipo</th>
                <th  scope="col">Departamento</th>
                <th  scope="col">Estado</th>
                <th  scope="col">Eliminado</th>
                <th  scope="col">Eliminar</th>

            </tr>
        </thead>
        <tbody>
            @forelse ($tickets as $ticket)
                <tr>
                    <td class="font-weight-bold"><a href="{{ route('ticket.show', $ticket) }}">{{ Str::limit($ticket->subject, 50, '...') }}</a></td>
                    <td class="font-weight-bold">{{ isset($ticket->equip) ? $ticket->equip->brand->model : '...' }}</td>
                    <td class="font-weight-bold">{{ isset($ticket->department) ? $ticket->department->name : '' }}</td>
                    <td class="font-weight-bold">{{ isset($ticket->state) ? $ticket->state->name : 'Null' }}</td>
                    <td class="font-weight-bold text-center">
                    @if ($ticket->deleted)
                    <i class="fas fa-ban" style="color:#ff0000;"></i>
                    @endif
                    </td>
                    <td class="font-weight-bold">
                        <form action="{{ route('admin.tickets.destroy', $ticket) }}" method="POST">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger" type="submit" onclick="return confirm('Está seguro?')"><i class="fas fa-trash-alt "></i></button>
                        </form>
                    </td>
                </tr>
            @empty
                    <tr>
                        <td>No hay ningún ticket.</td>
                    </tr>
                @endforelse

            </tbody>
        </table>
    </div>
    <div class="card-footer">
        {{ $tickets->links() }}
    </div>
    @else
    <div class="card-body">
        <strong>No hay registros</strong>
    </div>
    @endif
</div>
