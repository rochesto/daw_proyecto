<div>
    <h3 class="text-gray-700 text-center text-2xl font-bold mb-6">Comentarios</h3>
        @foreach ($comments as $comment)

            <x-card-comment>
                <x-slot name="unit">
                    {{ $comment->user->name }}
                </x-slot>
                <x-slot name="date">
                    {{ $comment->created_at->format('d M yy H:m') }}
                </x-slot>
                <x-slot name="icon">
                    {{ asset('img/icons/camping.png') }}
                </x-slot>

                {!! $comment->body !!}
            </x-card-comment>

        @endforeach

        <div class="mt-4 bg-white bg-gradient-to-b from-gray-200 shadow-md gap-4">
        <p class="text-gray-800 text-center font-bold mb-1"><span class="text-lg">Nuevo comentario</span></p>
            {!! Form::open(['route' => 'comments.store']) !!}
                @csrf
                {!! Form::hidden('ticket_id', $ticket->id) !!}
                {!! Form::textarea("body") !!}

                <script>CKEDITOR.replace( 'body' );</script>

                {!! Form::submit('Enviar', ['class' => 'bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded']) !!}
            {!! Form::close() !!}
        </div>
</div>
