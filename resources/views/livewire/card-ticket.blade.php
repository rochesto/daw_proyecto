<div>
    <div class="bg-white bg-gradient-to-b from-gray-100 rounded-lg shadow-md mb-5 grid grid-cols-3 gap-4">

        <div class="mt-2 px-3 col-span-2">
            <a href="{{ route('ticket.show', [$ticket->id]) }}" class="text-2xl text-gray-800 font-extrabold" >{{$ticket->subject}}</a>
        </div>
        <div class="text-right mr-3">
            {{ Form::model($ticket, array('route' => array('ticket.update', $ticket->id), 'method' => 'PUT')) }}
                @csrf
                {!! Form::select('state_id', $states, $ticket->state_id, ['class' => 'bg-gray-200 hover:bg-gray-400 font-bold py-2 px-4 rounded']) !!}
                {!! Form::submit('Cambiar estado', ['class' => 'bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded']) !!}
            {!! Form::close() !!}

        </div>
        <div class="col-span-3">
            <div class="p-3 mx-3 rounded-md bg-gray-200 font-bold grid grid-cols-2">
                <div>
                    <p>Estado: <span class="font-light">{{ $ticket->state_id ? $ticket->state->name : '' }}</span></p>
                    <p>Creado: <span class="font-light">{{ $ticket->created_at }}</span></p>
                    <p>Actulaizado: <span class="font-light">{{ $ticket->updated_at }}</span></p>
                </div>
                <div>
                    <p> Creado por: <span class="font-light">{{ $ticket->department_id  ? $ticket->department->name : ''  }}</span></p>
                    <p> Equipo: <span class="font-light"> {{ $ticket->equip_id  ? $ticket->equip->brand->name : ''  }}</span></p>
                    <p> Nº Serie: <span class="font-light"> {{ $ticket->equip_id  ? $ticket->equip->serie : '' }}</span></p>
                </div>
            </div>
        </div>
        <div class="col-span-2 px-3 mb-5">
            <p class="mt-2 text-gray-800 font-semibold">{!! $ticket->body !!}</p>
        </div>
        <div class="text-right mr-3">
            <form action="{{ route('ticket.destroy', $ticket) }}" method="POST">
                @csrf
                @method('delete')
                <button class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded" type="submit" onclick="return confirm('Estas seguro?')">Eliminar</button>
            </form> 
        </div>

    </div>
</div>
