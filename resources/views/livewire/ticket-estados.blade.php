<div>
            {{-- Menu --}}

    <x-tickets-submenu :states="$states" />


                    {{-- seccion principal --}}
    <section class="mt-10 max-w-7xl mx-auto px-4 sm:px-6 ">
        <h1 class="text-gray-700 text-center text-4xl font-bold mb-6">Tickets</h1>

        <div>
            @foreach ($tickets as $ticket)
            <x-card-ticket>
                <x-slot name="title">
                    {{ $ticket->asunto }}
                </x-slot>
                <x-slot name="unit">
                    {{ $ticket->unit->nombre }}
                </x-slot>
                <x-slot name="date">
                    {{ $ticket->updated_at->format('d M yy H:m') }}
                </x-slot>
                <x-slot name="icon">
                    {{ asset('img/icons/camping.png') }}
                </x-slot>
                <x-slot name="ticket_id">
                    {{ $ticket->id }}
                </x-slot>
                <x-slot name="state">
                    {{ $ticket->state->nombre }}
                </x-slot>
                {{ $ticket->cuerpo }}
            </x-post>
            @endforeach
        </div>
        <div class="mb-2">
            {{ $tickets->links() }}
        </div>
    </section>
</div>

