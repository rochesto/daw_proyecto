<x-app-layout>

    <section class="mt-10 max-w-7xl mx-auto px-4 sm:px-6 ">
        <h1 class="text-gray-700 text-center text-4xl font-bold mb-6">Novedades</h1>
        <a href="{{ route('notices.create') }}"><button class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded mb-3">
            Nueva novedad
        </button></a>
        @if (isset($notices))
            @foreach ($notices as $notice)
                @livewire('card-notices', ['notice' => $notice], key($notice->id))
            @endforeach
            <div class="mb-2">
                {{ $notices->links() }}
            </div>  
        @else
            No hay novedades
        @endif

    </section>

</x-app-layout>
