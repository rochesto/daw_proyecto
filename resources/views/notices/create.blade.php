<x-app-layout>

    <div class="mt-10 max-w-7xl mx-auto px-4 sm:px-6 ">
        <div class="bg-white bg-gradient-to-b from-gray-200 rounded-lg shadow-md mb-5">
            <div class="">
                <div class="w-full max-w-5xl p-3 mx-auto text-gray-800 font-bold">
                    {!! Form::open(['route' => 'notices.store']) !!}

                        <div class="my-2">
                            {!! Form::label('title', 'Titulo') !!}
                            {!! Form::text('title', null, ['class' => 'form-control rounded-2xl' . ($errors->has('title') ? 'is-invalid' : ''), 'size' => '50', 'placeholder' => 'Título']) !!}
                        </div>

                        <div class="my-2">
                            {!! Form::textarea('body', '', ['']) !!}
                            <script> CKEDITOR.replace( 'body' ); </script>
                        </div>

                        <div class="text-center">
                            {!! Form::label('publish', 'Pulicar') !!}
                            {!! Form::checkbox('publish', true, false) !!}
                            {!! Form::submit('Crear', ['class' => 'bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
