<!-- component post -->
<div class="bg-white rounded-lg shadow-md gap-4 mb-5">

    <div class="mt-2 p-3">
        <h3 class="text-2xl text-gray-800 font-extrabold" >{{$title}}</h3>
        <p class="mt-2 text-gray-800 font-semibold">{!! $slot !!}</p>
    </div>
    <div class="flex justify-between items-center mt-4 p-3">
        <span class="font-light text-gray-600">{{ $date }}</span>
        <div>
            <a class="flex items-center" href="#">
                <a class="flex items-center" href="#">
                    <img class="mx-4 w-10 h-10 object-cover rounded-full hidden sm:block" src="{{ $icon }}" alt="avatar">
                    <h1 class="text-gray-700 font-bold">{{ $autor }}</h1>
                </a>
            </a>
        </div>
    </div>
</div>
