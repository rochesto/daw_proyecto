<!-- component comment -->
<div class="bg-white bg-gradient-to-b from-gray-200 shadow-md gap-4">

    <div class="p-3">
        <p class="mt-2 text-gray-800 font-semibold">{{$slot}}</p>
    </div>
    <div class="flex justify-between items-center mt-4 p-3">
        <span class="font-light text-gray-600">{{ $date }}</span>
        <div>
            <a class="flex items-center" href="#">
                <a class="flex items-center" href="#">
                    <img class="mx-4 w-10 h-10 object-cover rounded-full hidden sm:block" src="{{ $icon }}" alt="avatar">
                    <h1 class="text-gray-700 font-bold">{{ $unit }}</h1>
                </a>
            </a>
        </div>
    </div>
    <span class=" "></span>
    
</div>
