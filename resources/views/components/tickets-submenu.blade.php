@props(['states'])

<nav x-data="{ open: false }" class="bg-white bg-gradient-to-b from-gray-100 border-b border-gray-100 shadow mt-3 flex justify-center items-baseline flex-wrap">
    <div class="max-w-7xl px-4 sm:px-6 lg:px-8">
        <div class="flex h-16">
            <div class="flex items-center ">
                {{-- Botones menu --}}
                <div class="relative" x-data="{open: false}">
                    <button class="block bg-white shadow hover:bg-gray-200 mr-2 h-12 rounded-lg px-2 sm:px-6 lg:px-2 overflow-hidden focus:outline-none" x-on:click="open = !open">
                        <i class="fas fa-list mr-1 text-sm"></i>
                        Estados
                        <i class="fas fa-chevron-down text-sm"></i>
                    </button>

                    <!-- Dropdown Body -->
                    <div class="absolute mt-2" x-show="open" x-on:click.away="open = false">

                        @foreach ($states as $state)
                        <button class="block bg-white shadow h-12 w-28 px-2 hover:bg-gray-200 sm:px-6 lg:px-2 overflow-hidden focus:outline-none" wire:click="$set('state_id', {{$state->id}})" x-on:click="open = false">
                            {{$state->nombre}}
                        </button>
                        @endforeach

                    </div>

                    <!-- // Dropdown Body -->
                </div>
                    <!-- // Dropdown -->
                    <button class="cursor-pointer bg-white shadow hover:bg-gray-200 h-12 mr-2 rounded-lg px-2 sm:px-6 lg:px-2 overflow-hidden focus:outline-none" wire:click="$set('state_id', 0)">
                        Todos
                    </button>
                    <button class="cursor-pointer bg-white shadow hover:bg-gray-200 h-12 mr-2 rounded-lg px-2 sm:px-6 lg:px-2 overflow-hidden focus:outline-none">
                        <a href="{{ route('ticket.create') }}">Nuevo</a>
                    </button>
                {{-- // Botones menu --}}

            </div>
        </div>
    </div>

</nav>


