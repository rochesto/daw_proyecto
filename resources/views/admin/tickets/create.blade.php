@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Crear rol</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            {!! Form::open(['route' => 'admin.roles.store']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'nombre') !!}
                    {!! Form::text('name', null, ['class' => 'form-control ' . ($errors->has('name') ? 'is-invalid' : ''), 'placeholder' => 'nuevo permiso']) !!}
                    @error('name')
                        <span class="invalid-feedback">
                            Se necesita un nombre.
                        </span>
                    @enderror
                </div>
                <strong>Permisos</strong>
                <br>
                    @error('permissions')
                        <span class="text-danger">
                            Añade al menos un permiso.
                        </span>
                    @enderror
                @foreach ($permissions as $permis)
                <div>

                    {!! Form::checkbox('permissions[]', $permis->id, null, ['class' => 'mr-1']) !!}
                    {{ $permis->name }}
                </div>
                @endforeach
                {!! Form::submit('Crear', ['class' => 'btn btn-primary mt-2']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop
