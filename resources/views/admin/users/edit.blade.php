@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Editar Usuario</h1>
@stop

@section('content')
    <div class="card">
        @if (session('info'))
            <div class="alert alert-primary" role="alert">
                <strong>Éxito!</strong> {{ session('info') }}
            </div>
        @endif

        <div class="card-body">
            {!! Form::model($user, ['route' => ['admin.users.update', $user], 'method' => 'put']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'nombre') !!}
                    {!! Form::text('name', null, ['class' => 'form-control ' . ($errors->has('name') ? 'is-invalid' : '')]) !!}
                    @error('name')
                        <span class="invalid-feedback">
                            Se necesita un nombre.
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Correo') !!}
                    {!! Form::email('email', null, ['class' => 'form-control ' . ($errors->has('email') ? 'is-invalid' : '')]) !!}
                    @error('email')
                        <span class="invalid-feedback">
                            Se necesita un correo.
                        </span>
                    @enderror
                </div>
                <strong>Department</strong>
                <br>
                {!! Form::select('department_id', $departments, $user->department_id) !!}
                <br>

                <strong>Roles</strong>
                @error('roles')
                    <span class="text-danger">
                        Lista de roles.
                    </span>
                @enderror
                @foreach ($roles as $rol)
                <div>

                    {!! Form::checkbox('roles[]', $rol->id, null, ['class' => 'mr-1']) !!}
                    {{ $rol->name }}
                </div>
                @endforeach
                {!! Form::submit('Editar', ['class' => 'btn btn-primary mt-2']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop
