@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Crear Usuario</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            {!! Form::open(['route' => 'admin.users.store']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'nombre') !!}
                    {!! Form::text('name', null, ['class' => 'form-control ' . ($errors->has('name') ? 'is-invalid' : ''), 'placeholder' => 'nombre']) !!}
                    @error('name')
                        <span class="invalid-feedback">
                            Se necesita un nombre.
                        </span>
                    @enderror
                    {!! Form::label('email', 'email') !!}
                    {!! Form::email('email', null, ['class' => 'form-control ' . ($errors->has('email') ? 'is-invalid' : ''), 'placeholder' => 'email']) !!}
                    @error('email')
                        <span class="invalid-feedback">
                            Se necesita un email valido.
                        </span>
                    @enderror
                    {!! Form::label('password', 'password') !!}
                    {!! Form::password('password', null, ['class' => 'form-control ' . ($errors->has('password') ? 'is-invalid' : '')]) !!}
                    @error('password')
                        <span class="invalid-feedback">
                            Se necesita un password valido.
                        </span>
                    @enderror

                    {{-- Unidades --}}
                    <br>
                    {!! Form::label('department_id', 'Departamento') !!}
                    {!! Form::select('department_id', $departments, null, ['class' => '']) !!}
                    {{-- roles --}}
                    <br>
                    <strong>Roles</strong>
                    @foreach ($roles as $rol)
                    <div>
                        {!! Form::checkbox('roles[]', $rol->id, null, ['class' => 'mr-1']) !!}
                        {{ $rol->name }}
                    </div>
                    @endforeach
                </div>
                    {!! Form::submit('Crear', ['class' => 'btn btn-primary mt-2']) !!}

            {!! Form::close() !!}
        </div>
    </div>
@stop
