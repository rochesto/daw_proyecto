@extends('adminlte::page')

@section('title', 'Usuarios')

@section('content_header')
    Usuarios
@stop

@section('content')
    @livewire('admin-users')
@stop

