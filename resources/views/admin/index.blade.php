@extends('adminlte::page')

@section('title', 'Ticket DAW')

@section('content')

    <div class="card" style="width: 28rem;">
        <div class="card-header text-white bg-dark mb-3 ">
            Tickets
        </div>
        <div class="card-body container">
            <div class="row">
                <div class="col-sm">
                    <p></p>
                    <p><i class="fas fa-folder-open"></i> Abiertos: {{ $datos['tickets']['abiertos'] }}</p>
                    <p><i class="fas fa-check-circle"></i> Cerrados: {{ $datos['tickets']['terminados'] }}</p>
                </div>
                <div class="col-sm">
                    Último Ticket
                    <p><a href="{{ route('ticket.show', $datos["tickets"]["ultimo"]) }}">{{$datos["tickets"]["ultimo"]->subject}}</a></p>
                </div>
            </div>
        </div>
    </div>

@stop


