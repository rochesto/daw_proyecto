@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Nueva unidad</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            {!! Form::open(['route' => 'admin.departments.store']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'nombre') !!}
                    {!! Form::text('name', null, ['class' => 'form-control ' . ($errors->has('name') ? 'is-invalid' : ''), 'placeholder' => 'nombre unidad']) !!}
                    {!! Form::label('email', 'email') !!}
                    {!! Form::email('email', null, ['class' => 'form-control ' . ($errors->has('email') ? 'is-invalid' : ''), 'placeholder' => 'email']) !!}
                    {!! Form::label('phone', 'telefono') !!}
                    {!! Form::text('phone', null, ['class' => 'form-control ' . ($errors->has('phone') ? 'is-invalid' : ''), 'placeholder' => 'telefono']) !!}
                    @error('name')
                        <span class="invalid-feedback">
                            Se necesita un nombre.
                        </span>
                    @enderror
                </div>
                {!! Form::submit('Crear', ['class' => 'btn btn-primary mt-2']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop
