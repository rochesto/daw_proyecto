@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Editar Unidad</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            {!! Form::model($department, ['route' => ['admin.departments.update', $department], 'method' => 'put']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'name') !!}
                    {!! Form::text('name', null, ['class' => 'form-control ' . ($errors->has('name') ? 'is-invalid' : '')]) !!}
                    @error('name')
                        <span class="invalid-feedback">
                            Se necesita un nombre.
                        </span>
                    @enderror
                    {!! Form::label('email', 'email') !!}
                    {!! Form::email('email', null, ['class' => 'form-control ' . ($errors->has('email') ? 'is-invalid' : '')]) !!}
                    @error('email')
                        <span class="invalid-feedback">
                            Se necesita un correo.
                        </span>
                    @enderror
                    {!! Form::label('phone', 'telefono') !!}
                    {!! Form::text('phone', null, ['class' => 'form-control ' . ($errors->has('phone') ? 'is-invalid' : '')]) !!}
                    @error('phone')
                        <span class="invalid-feedback">
                            Se necesita un telefono.
                        </span>
                    @enderror
                </div>
                {!! Form::submit('Editar', ['class' => 'btn btn-primary mt-2']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop
