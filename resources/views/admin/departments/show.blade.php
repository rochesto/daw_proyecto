@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')


    <table class="overflow-x-auto w-full bg-white divide-y divide-gray-200">
        <thead class="bg-gray-50 text-gray-500 text-m">
            <tr class="divide-x divide-gray-300">
                <th class="px-3 py-2 flex justify-center text-left text-m text-gray-500 uppercase">id</th>
                <th class="px-3 py-2 flex justify-center text-left text-m text-gray-500 uppercase">nombre</th>
                <th class="px-3 py-2 flex justify-center text-left text-m text-gray-500 uppercase">Editar</th>
                <th class="px-3 py-2 flex justify-center text-left text-m text-gray-500 uppercase">Eliminar</th>
            </tr>
        </thead>
        <tbody class="text-gray-500 text-m divide-y divide-gray-200">
            <tr class="text-center">
            <td class="py-3 flex justify-center">{{ $permission->id }}</td>
                <td class="font-weight-bold">{{ $permission->name }}</td>
                <td class="font-weight-bold">
                    <button class="btn btn-info">
                        <a href="{{ route('admin.permissions.edit', $permission) }}"><i class="fas fa-edit"></i></a>
                    </button>
                </td>
                <td class="font-weight-bold">
                    <form action="{{ route('admin.permissions.destroy', $permission) }}" method="POST">
                        @csrf
                        @method('delete')
                        <button class="btn btn-danger" type="submit"><i class="fas fa-trash-alt "></i></button>
                    </form>
                </td>
            </tr>
        </tbody>
    </table>
@stop
