@extends('adminlte::page')

@section('title', 'Panel Administracion')

@section('content_header')
    <h1>Departamentos</h1>
@stop

@section('content')
    <a href="{{ route('admin.departments.create') }}"><button class="btn btn-outline-dark mb-3">Nuevo departamento</button></a>

    @if (session('info'))
        <div class="alert alert-primary" role="alert">
            <strong>Éxito!</strong> {{ session('info') }}
        </div>

    @endif

    <table class="table table-striped">
        <thead>
            <tr class="bg-primary">
                <th  scope="col">id</th>
                <th  scope="col">nombre</th>
                <th  scope="col">Email</th>
                <th  scope="col">Telefono</th>
                <th  scope="col">Editar</th>
                <th  scope="col">Eliminar</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($departments as $department)
                <tr>
                    <td>{{ $department->id }}</td>
                    <td>{{ $department->name }}</td>
                    <td>{{ $department->email }}</td>
                    <td>{{ $department->phone }}</td>
                    <td>
                        <a href="{{ route('admin.departments.edit', $department) }}"><button class="btn btn-info">
                            <i class="fas fa-edit"></i>
                        </button></a>
                    </td>
                    <td>
                        <form action="{{ route('admin.departments.destroy', $department) }}" method="POST">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger" type="submit" onclick="return confirm('Estas seguro?')"><i class="fas fa-trash-alt "></i></button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>No hay ningún rol.</td>
                </tr>
            @endforelse

        </tbody>
    </table>
@stop

