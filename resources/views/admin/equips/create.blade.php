@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Nuevo Equipo</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            {!! Form::open(['route' => 'admin.equips.store']) !!}
                <div class="form-group">
                    {!! Form::label('serie', 'serie') !!}
                    {!! Form::text('serie', null, ['class' => 'form-control ' . ($errors->has('serie') ? 'is-invalid' : ''), 'placeholder' => 'nombre unidad']) !!}
                    @error('serie')
                        <span class="invalid-feedback">
                            Se necesita un número de serie.
                        </span>
                    @enderror
                    {!! Form::label('mac', 'mac') !!}
                    {!! Form::text('mac', null, ['class' => 'form-control ' . ($errors->has('mac') ? 'is-invalid' : ''), 'placeholder' => 'email']) !!}
                    {!! Form::label('ip', 'ip') !!}
                    {!! Form::text('ip', null, ['class' => 'form-control ' . ($errors->has('ip') ? 'is-invalid' : ''), 'placeholder' => 'telefono']) !!}
                    <br />
                    {!! Form::label('brand_id', 'Marca') !!}
                    {!! Form::select('brand_id', $brands, null) !!}
                    <br />
                    {!! Form::label('department_id', 'Departamento') !!}
                    {!! Form::select('department_id', $departments, null) !!}
                </div>
                {!! Form::submit('Crear', ['class' => 'btn btn-primary mt-2']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop
