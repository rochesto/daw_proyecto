@extends('adminlte::page')

@section('title', 'Equipos')

@section('content_header')
    <h1>Editar Equipo</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            {!! Form::model($equip, ['route' => ['admin.equips.update', $equip], 'method' => 'put']) !!}
                <div class="form-group">
                    {!! Form::label('serie', 'Serie') !!}
                    {!! Form::text('serie', null, ['class' => 'form-control ' . ($errors->has('serie') ? 'is-invalid' : '')]) !!}
                    @error('serie')
                        <span class="invalid-feedback">
                            Se necesita un número se serie.
                        </span>
                    @enderror
                    {!! Form::label('mac', 'MAC') !!}
                    {!! Form::text('mac', null, ['class' => 'form-control ' . ($errors->has('mac') ? 'is-invalid' : '')]) !!}
                    @error('mac')
                        <span class="invalid-feedback">
                            Se necesita una MAC.
                        </span>
                    @enderror
                    <strong>Marca</strong>
                    <br>
                    {!! Form::select('brand_id', $brands, $equip->unit_id) !!}
                    <br>
                    <strong>Departamento</strong>
                    <br>
                    {!! Form::select('department_id', $departments, $equip->department_id) !!}
                    <br>

                </div>
                {!! Form::submit('Editar', ['class' => 'btn btn-primary mt-2']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop
