@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Equipos</h1>
@stop

@section('content')
    @livewire('admin-equips')
@stop

