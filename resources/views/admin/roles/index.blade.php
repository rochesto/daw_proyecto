@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

@stop

@section('content')
<a href="{{ route('admin.roles.create') }}"><button class="btn btn-outline-dark mb-3">Nuevo Rol</button></a>

    @if (session('info'))
        <div class="alert alert-primary" role="alert">
            <strong>Éxito!</strong> {{ session('info') }}
        </div>

    @endif

    <table class="table table-striped">
        <thead>
            <tr class="bg-primary">
                <th  scope="col">id</th>
                <th  scope="col">nombre</th>
                <th  scope="col">Editar</th>
                <th  scope="col">Eliminar</th>
                <th  scope="col">Permisos</th>

            </tr>
        </thead>
        <tbody>
            @forelse ($roles as $role)
                <tr>
                    <td>{{ $role->id }}</td>
                    <td>{{ $role->name }}</td>
                    <td>
                        <a href="{{ route('admin.roles.edit', $role) }}"><button class="btn btn-info">
                            <i class="fas fa-edit"></i>
                        </button></a>
                    </td>
                    <td>
                        <form action="{{ route('admin.roles.destroy', $role) }}" method="POST">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger" type="submit" onclick="return confirm('Está seguro?')"><i class="fas fa-trash-alt "></i></button>
                        </form>
                    </td>
                    <td>
                        @foreach ($role->permissions as $permission)
                            {{ $permission->name }},
                        @endforeach
                    </td>
                </tr>
            @empty
                <tr>
                    <td>No hay ningún rol.</td>
                </tr>
            @endforelse

        </tbody>
    </table>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
